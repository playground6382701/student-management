from typing_extensions import Annotated
from pydantic import ValidationError
from fastapi import APIRouter, Depends, Form
from fastapi.requests import Request
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from src.payloads import CreateSubjectPayload, GetSubjectPayload, UpdateSubjectPayload
from src.controllers import subjects as controllers
from src.controllers import departments as dpm_controllers


router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.post("/create_subject")
async def create_subject_form(
    request: Request, 
    name: str = Form(),
    codename: str = Form(),
    department: int = Form(),
    credit: int = Form(),
):
    context = {
            "request": request,   
        }
    try:
        payload = CreateSubjectPayload(
            name=name,
            codename=codename,
            department_id=department,
            credit=credit,
        )
    except ValidationError as e:
        print(e)
        context = {
            "request": request,
            "is_error": True,
            "errors": {
                "msg": "Thông tin nhập vào không thỏa điểu kiện",
                "name": "Tên khoa chỉ được chứa 64 ký tự",
            },
        }
        return templates.TemplateResponse("subject/create_subject.html", context=context)
    
    subject = controllers.get_subject({"codename": payload.codename})

    departments = dpm_controllers.get_department()
    context.update({
        "departments": departments
    })
    if subject:
        context.update({
            "is_error": True,
            "errors": {
                "msg": f"Đã tồn tại môn với mã {payload.codename}",
            }
        })
        return templates.TemplateResponse("subject/create_subject.html", context=context)
    
    
    try:
        subject = controllers.create_subject(payload)
    except Exception as e:
        print(e)
        context.update({
            "is_error": True,
            "errors": {
                "msg": e,
            }
        })
        return templates.TemplateResponse("subject/create_subject.html", context=context)

    return RedirectResponse("/get_subject", 303)


@router.get("/create_subject")
async def create_subject(request: Request):
    departments = dpm_controllers.get_department()
    context = {
        "request": request,
        "is_error": False,
        "departments": departments,
    }
    return templates.TemplateResponse("subject/create_subject.html", context=context)


@router.get("/get_subject")
async def get_subject(request: Request):
    data = controllers.get_subject()
    context = {
        "request": request, 
        "data": data,
    }
    return templates.TemplateResponse("subject/get_subject.html", context)

@router.get("/update_subject/{id}", status_code=200)
async def update_subject(request: Request, id: int):
    departments = dpm_controllers.get_department()
    context = {
        "request": request,
        "departments": departments,
    }
    query_payload = GetSubjectPayload(id=id)
    data = controllers.get_subject({"id": query_payload.id})
    context["data"] = data[0]
    return templates.TemplateResponse("subject/update_subject.html", context)

@router.post("/update_subject/{id}", status_code=200)
async def update_subject_form(
    request: Request,
    id: int,
    name: str = Form(),
    codename: str = Form(),
    department: str = Form(),
    credit: int = Form(),
):
    context = {
        "request": request,
    }
    payload = UpdateSubjectPayload(
        id=id,
        name=name, 
        codename=codename, 
        department_id=department,
        credit=credit,
    )
    if False:
        subjects = controllers.get_subject({"id": id})
        data = controllers.get_subject({"id": id})
        context.update({
            "subject": subjects[0],
            "is_error": True,
            "errors": {
                "msg": f"Đã có khoa có tên {payload.codename}. Vui lòng chọn tên khác"
            },
            "data": data[0],
        })
        return templates.TemplateResponse("subject/update_subject.html", context)
    controllers.update_subject(payload)
    return RedirectResponse("/get_subject", 303)


@router.post("/delete_subject/{id}", status_code=204)
async def delete_subject_form(request: Request, id: int):
    controllers.delete_subject(id)
    return RedirectResponse("/get_subject", 303)


@router.get("/get_subject_detail/{id}")
async def get_subject_detail(request: Request, id: int):
    results = controllers.get_subject_detail(id)
    subject = controllers.get_subject({"id": id})
    context = {
        "request": request,
        "data": results,
        "subject": subject,
    }
    return templates.TemplateResponse("subject/get_subject_detail.html", context=context)


@router.get("/update_student_grade/{student_id}/{subject_id}")
async def update_student_grade(request: Request, student_id: int, subject_id: int):
    student_subject = controllers.get_grade({"student_id": student_id, "subject_id": subject_id})
    context = {
        "request": request,
        "student_id": student_id,
        "subject_id": subject_id,
        "student_subject": student_subject
    }
    return templates.TemplateResponse("subject/update_student_grade.html", context=context)


@router.post("/update_student_grade/{student_id}/{subject_id}")
async def update_student_grade_form(
    request: Request, 
    student_id: int, 
    subject_id: int,
    grade: float = Form(...),
    note: str = Form(""),
):
    context = {
        "request": request
    }
    if grade < 0 or grade > 10:
        student_subject = controllers.get_grade({"student_id": student_id, "subject_id": subject_id})
        context.update({
            "is_error": True,
            "errors": {
                "msg": "Điểm chỉ được nằm trong khoảng từ 0-10"
            },
            "student_id": student_id,
            "subject_id": subject_id,
            "student_subject": student_subject,
        })
        return templates.TemplateResponse("subject/update_student_grade.html", context=context)
    controllers.update_student_grade(student_id, subject_id, grade, note)
    return RedirectResponse(f"/get_subject_detail/{subject_id}", 303)