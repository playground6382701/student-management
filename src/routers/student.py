from typing_extensions import Annotated
from fastapi import APIRouter, Depends, Form, UploadFile, File, Query
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.requests import Request
from fastapi.templating import Jinja2Templates

from src.payloads import ResponsePayload, CreateStudentPayload, ListStudentPayload, UpdateStudentPayload
from src.controllers import students as controllers
from src.controllers import departments as dpm_controllers
from src.controllers import email as mail_controller
from src.controllers import subjects as sub_controllers


router = APIRouter()

templates = Jinja2Templates(directory="templates")


@router.get("/create_student")
async def create_student(request: Request):
    departments = dpm_controllers.get_department()
    context = {
        "request": request,
        "departments": departments
    }
    return templates.TemplateResponse("student/create_student.html", context)


@router.post("/create_student")
async def create_student_form(
    request: Request,
    username: str = Form(...),
    first_name: str = Form(...),
    last_name: str = Form(...),
    email: str = Form(...),
    department: int = Form(...),
):
    context = {
            "request": request,
        }
    try:
        payload = CreateStudentPayload(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            department=department,
        )
    except ValueError as e:
        print(e)
        context.update({
            "is_error": True,
            "errors": {
                "msg": "Dự liệu đã nhập không thỏa điều kiện",
                "email": "Email phải đúng định dạng sample@sample.sample"
            }
        })
        return templates.TemplateResponse("student/create_student.html", context=context)
    try:
        await controllers.create_student(payload)
    except Exception as e:
        print(e)
        context.update({
            "is_error": True,
            "errors": {
                "msg": e,
            }
        })
        return templates.TemplateResponse("student/create_student.html", context=context)

    return RedirectResponse("/get_student", 303)

@router.get("/get_student")
async def get_student(
    request: Request, 
    ):
    data = controllers.get_student()
    context = {
        "request": request,
        "data": data
    }
    return templates.TemplateResponse("student/get_student.html", context)


@router.get("/update_student/{id}")
async def update_student(request: Request):
    data = controllers.get_student()
    departments = dpm_controllers.get_department()
    context = {
        "request": request,
        "data": data[0],
        "departments": departments,
    }
    return templates.TemplateResponse("student/update_student.html", context)


@router.post("/update_student/{id}")
async def update_student_form(
        request: Request,
        id: int,
        file: UploadFile = File(None),
        first_name: str = Form(...),
        last_name: str = Form(...),
        department: int = Form(...),
    ):

    context = {
            "request": request,
        }
    try:
        payload = UpdateStudentPayload(
            id=id,
            first_name=first_name,
            last_name=last_name,
            department=department,
            file=file.file if file else file
        )
    except ValueError as e:
        print(e)
        departments = dpm_controllers.get_department()
        context.update({
            "is_error": True,
            "errors": {
                "msg": "Dự liệu đã nhập không thỏa điều kiện",
                "email": "Email phải đúng định dạng sample@sample.sample",
                "departments": departments
            }
        })
        return templates.TemplateResponse("student/update_student.html", context=context)
    
    controllers.update_student(payload=payload)
    return RedirectResponse("/get_student", 303)


@router.post("/delete_student/{id}")
async def delete_student_form(request: Request, id: int):
    controllers.deactivate_student(id)
    return RedirectResponse("/get_student", 303)


@router.get("/list_student_subject/{id}")
async def list_student_subject(
    request: Request,
    id: int,
):
    context = {
        "request": request
    }
    subjects = sub_controllers.get_student_subject(id)
    context.update({
      "data": subjects,
    })
    return templates.TemplateResponse("student/list_subject.html", context=context)
