from typing_extensions import Annotated
from fastapi import APIRouter, Depends, Form
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.requests import Request
from fastapi.templating import Jinja2Templates

from src.payloads import ActivateUserPayload
from src.controllers import user as controller


router = APIRouter()

templates = Jinja2Templates(directory="templates")


@router.get("/activate_user/{activation_code}")
async def activate_user(
    request: Request,
    activation_code: str,
):
    context = {
        "request": request,
        "activation_code": activation_code
    }
    return templates.TemplateResponse("user/activate_user.html", context)


@router.post("/activate_user/{activation_code}")
async def activate_user_form(
        request: Request,
        activation_code: str,
        password: str = Form(...),
        confirm_password: str = Form(...)
    ):
    payload = ActivateUserPayload(
        activation_code=activation_code,
        password=password,
        confirm_password=password,
    )
    try:
        controller.activate_user(payload)
    except Exception as e:
        context = {
            "request": request,
            "is_error": True,
            "errors": {
                "msg": "Không thể kích hoạt tài khoản. Vui lòng thử lại"
            }
        }
        return templates.TemplateResponse("user/activate_user.html", context)    

    context = {
        "request": request,
        "is_activated": True,
    }
    return templates.TemplateResponse("user/activate_user.html", context)