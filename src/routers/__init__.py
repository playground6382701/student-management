from .student import router as student_router
from .department import router as department_router
from .subject import router as subject_router
from .user import router as user_router
from .register import router as register_router

ROUTERS = [
    student_router,
    department_router,
    subject_router,
    user_router,
    register_router
]