from typing import List
from pydantic import ValidationError
from fastapi import APIRouter, Depends, Form
from fastapi.requests import Request
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from src.controllers import register as controllers
from src.controllers import departments as dpm_controllers
from src.controllers import subjects as sub_controllers
from src.payloads import StudentSubjectPayload
router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.get("/register/{studentID}")
async def subject_register(request: Request, studentID: int):
  department = dpm_controllers.get_department_by_student_id(studentID)
  context = {
    "request": request,
    "student_id": studentID,
  }
  if department:
    subjects = sub_controllers.get_student_subject(studentID)
    context.update({
      "subjects": subjects,
      "departmentName": department.name,
    })
  return templates.TemplateResponse("register/register.html", context)

@router.post("/register/{studentID}")
async def subject_register_form(
    request: Request,
    studentID: int,
    subjectID: List[int] = Form(...),
):
    payload = {
        "subjectID": subjectID,
        "studentID": studentID
    }
    context = {
        "request": request,
    }
    print(subjectID)
    data = controllers.register_subject(payload)
    context["data"] = data
    return RedirectResponse("/get_subject", 303)