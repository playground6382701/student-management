from typing_extensions import Annotated
from pydantic import ValidationError
from fastapi import APIRouter, Depends, Form
from fastapi.requests import Request
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from src.payloads import CreateDepartmentPayload, GetDepartmentPayload, UpdateDepartmentPayload
from src.controllers import departments as controllers


router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.post("/create_department")
async def create_department_form(
    request: Request, 
    name: str = Form(),
    codename: str = Form(),
    required_credit: str = Form(),
):
    try:
        payload = CreateDepartmentPayload(name=name, codename=codename, required_credit=required_credit)
    except ValidationError as e:
        print(e)
        context = {
            "request": request,
            "is_error": True,
            "errors": {
                "msg": "Thông tin nhập vào không thỏa điểu kiện",
                "name": "Tên khoa chỉ được chứa 32 ký tự",
                "codename": "Tên mã chỉ được chứa 4 ký tự",
            },
        }
        return templates.TemplateResponse("department/create_department.html", context=context)

    is_existed_codename = controllers.is_department_existed(payload.codename)
    if not is_existed_codename:
        department = controllers.create_department(payload)
        return RedirectResponse("/get_department", 303)
    else:
        context = {
            "request": request,
            "is_error": True,
            "errors": {
                "msg": f"Đã tồn tại khoa có mã hiệu {payload.codename}",
            },
        }
        return templates.TemplateResponse("department/create_department.html", context=context)


@router.get("/create_department")
async def create_department(request: Request):
    context = {
        "request": request,
        "is_error": False,
    }
    return templates.TemplateResponse("department/create_department.html", context=context)


@router.get("/get_department")
async def get_department(request: Request):
    departments = controllers.get_department()
    context = {
        "request": request,
        "departments": departments
    }
    print(context)
    return templates.TemplateResponse("department/get_department.html", context)

@router.get("/update_department/{id}", status_code=200)
async def update_department(request: Request, id: int):
    context = {
        "request": request,
    }
    query_payload = GetDepartmentPayload(id=id)
    departments = controllers.get_department({"id": query_payload.id})
    context["department"] = departments[0]
    return templates.TemplateResponse("department/update_department.html", context)

@router.post("/update_department/{id}", status_code=200)
async def update_department_form(
    request: Request,
    id: int,
    name: str = Form(),
    codename: str = Form(),
    required_credit: str = Form(),
    ):
    context = {
        "request": request,
    }
    payload = UpdateDepartmentPayload(id=id, name=name, codename=codename, required_credit=required_credit)
    departments = controllers.get_department({
        "codename": payload.codename,
    })
    if departments and departments[0].id != payload.id:
        departments = controllers.get_department({"id": payload.id})
        context.update({
            "department": departments[0],
            "is_error": True,
            "errors": {
                "msg": f"Đã có khoa có tên {payload.codename}. Vui lòng chọn tên khác"
            }
        })
        return templates.TemplateResponse("department/update_department.html", context)
    controllers.update_department(payload)
    return RedirectResponse("/get_department", 303)


@router.post("/delete_department/{id}", status_code=204)
async def delete_department_form(request: Request, id: int):
    controllers.delete_department(id)
    return RedirectResponse("/get_department", 303)