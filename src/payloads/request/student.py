from typing import Optional, List, Any
from pydantic import BaseModel, Field, EmailStr
from fastapi import Query


class CreateStudentPayload(BaseModel):
    email: EmailStr
    username: str
    first_name: str
    last_name: str
    department: int


class ListStudentPayload(BaseModel):
    page_limit: int = 10
    page_index: int = 0


class UpdateStudentPayload(BaseModel):
    id: int
    first_name: str
    last_name: str
    department: int
    file: Any