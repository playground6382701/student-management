from pydantic import BaseModel, model_validator


class ActivateUserPayload(BaseModel):
    activation_code: str
    password: str
    confirm_password: str

    @model_validator(mode="after")
    def check_password_match(self):
        if self.password != self.confirm_password:
            raise ValueError("Password not match")
        return True