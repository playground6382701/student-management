from typing import Optional, List
from pydantic import BaseModel, Field
from fastapi import Query, Form


class CreateSubjectPayload(BaseModel):
    name: str = Field(max_length=32)
    codename: str = Field(max_length=4)
    credit: int
    department_id: int


class UpdateSubjectPayload(BaseModel):
    id: int
    name: str = Field(max_length=64)
    codename: str = Field(max_length=4)
    credit: int
    department_id: int


class GetSubjectPayload(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    codename: Optional[str] = None
    department_id: Optional[int] = None


class StudentSubjectPayload(BaseModel):
    studentID: str
    departmentID: str
    subjectID: List[int]