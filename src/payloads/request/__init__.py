from .departments import *
from .student import *
from .subjects import *
from .users import *