from typing import Optional, List
from pydantic import BaseModel, Field
from fastapi import Query, Form


class CreateDepartmentPayload(BaseModel):
    name: str = Field(max_length=32)
    codename: str = Field(max_length=4)
    required_credit: int


class UpdateDepartmentPayload(BaseModel):
    id: int
    name: str = Field(max_length=32)
    codename: str = Field(max_length=4)
    required_credit: int

class GetDepartmentPayload(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    codename: Optional[str] = None