from typing import List, Any
from pydantic import BaseModel


class ResponsePayload(BaseModel):
    data: List[Any] = []
    is_error: bool = False
    page_index: int = 0
    page_limit: int = 10
    total_query: int = 0