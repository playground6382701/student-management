from typing import List
from sqlalchemy import ForeignKey, inspect
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from sqlalchemy.types import String, Numeric, Boolean, DateTime, Integer, Float, Text


class Base(DeclarativeBase):
    def _asdict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

                                                          
class User(Base):
    __tablename__ = "user"
    id: Mapped[int] = mapped_column(primary_key=True)
    code: Mapped[str] = mapped_column(String(64), unique=True)
    email: Mapped[str] = mapped_column(String(64), unique=True)
    username: Mapped[str] = mapped_column(String(16), unique=True)
    password: Mapped[str] = mapped_column(String(64))
    first_name: Mapped[str] = mapped_column(String(64))
    last_name: Mapped[str] = mapped_column(String(64))
    is_active: Mapped[bool] = mapped_column(Boolean, default=False)

    student_info: Mapped["Student"] = relationship(lazy=False)


class Student(Base):
    __tablename__ = "student"

    id: Mapped[int] = mapped_column(primary_key=True)
    is_graduated: Mapped[bool] = mapped_column(Boolean, default=False)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    department_id: Mapped[int] = mapped_column(ForeignKey("department.id"))
    profile_image_url:  Mapped[str] = mapped_column(Text(), nullable=True)

class StudentSubject(Base):
    __tablename__ = "student_subject"

    id: Mapped[int] = mapped_column(primary_key=True)
    grade: Mapped[int] = mapped_column(Float, nullable=True)
    note: Mapped[str] = mapped_column(Text(), default="")

    student_id: Mapped[int] = mapped_column(ForeignKey("student.id"))
    subject_id: Mapped[int] = mapped_column(ForeignKey("subject.id"))


class Teacher(Base):
    __tablename__ = "teacher"

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))


class Employee(Base):
    __tablename__ = "employee"

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))


class Department(Base):
    __tablename__ = "department"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(32))
    codename: Mapped[str] = mapped_column(String(4), unique=True)
    code: Mapped[str] = mapped_column(String(32), unique=True)
    required_credit: Mapped[int] = mapped_column(Integer)

    subjects: Mapped[List["Subject"]] = relationship(back_populates="department")


class Subject(Base):
    __tablename__ = "subject"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(64))
    codename: Mapped[str] = mapped_column(String(4), unique=True)
    credit: Mapped[int] = mapped_column(Integer)
    
    department_id: Mapped[int] = mapped_column(ForeignKey("department.id"))
    department: Mapped["Department"] = relationship(back_populates="subjects")


class Class(Base):
    __tablename__ = "class"
    id: Mapped[int] = mapped_column(primary_key=True)


# TODO: add all other schemas before develop
SCHEMAS = [
    Department,
    Subject,
    User,
    Student,
]