from src.connections import postgresql
from src.schemas import SCHEMAS



def _define_database():
    engine = postgresql.get_engine()
    for schema in SCHEMAS:
        # TODO: delete drop all line
        # schema.metadata.drop_all(engine)
        schema.metadata.create_all(engine)

def setup_startup():
    _define_database()