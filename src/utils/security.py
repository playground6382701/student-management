from passlib.context import CryptContext
from cryptography import fernet
import json
import time

from settings import settings


ALGORITHM = "HS256"


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password, hashed_password, raise_exc:bool=False):
    is_matched = pwd_context.verify(plain_password, hashed_password)
    if raise_exc and is_matched is False:
        raise Exception("wrong password")
    return is_matched
    


def get_password_hash(password):
    hashed_password = pwd_context.hash(password)
    return hashed_password


def create_activation_code(data: dict, expire_at: int = None):
    f = fernet.Fernet(settings.SECRET_KEY)
    if expire_at is None:
        expire_at = time.time() + 1000 * 60 * 60 * 24
    value = json.dumps({
        **data,
        "expire_at": expire_at,
    })
    encrypted = f.encrypt(bytes(value, encoding="utf-8"))
    encrypted = encrypted.decode()
    return encrypted

def decrypt_activation_code(code: str):
    f = fernet.Fernet(settings.SECRET_KEY)
    decripted = f.decrypt(code).decode()
    decripted = json.loads(decripted)
    return decripted