from typing import List
from uuid import uuid1

from src.connections import postgresql
from src.schemas import Student, StudentSubject, Subject, User, Department
from src.payloads.request import CreateStudentPayload
from src.utils.security import get_password_hash, create_activation_code
from src.payloads import ListStudentPayload
from .email import send_email, render_email_from_jinja2
from settings import settings
from pydantic import BaseModel, Field, EmailStr
from src.payloads import StudentSubjectPayload

def register_subject(payload: dict):
    session = postgresql.get_session()
    try:
        for subjectID in payload.get("subjectID", []):
            studentSubject = StudentSubject(
                student_id=payload["studentID"],
                subject_id=subjectID
            )
            session.add(studentSubject)
            session.flush()
    except Exception as e:
        print(e)
    else:
        session.commit()