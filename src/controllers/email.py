from typing import List, Any
from fastapi_mail import MessageSchema, MessageType
from jinja2 import Environment, FileSystemLoader

from src.connections.email import mail


jinja_env = Environment(loader=FileSystemLoader('templates'))


async def send_email(to_addr: List[str] | str, subject: str, body: str):
    if isinstance(to_addr, str):
        to_addr = [to_addr]

    message = MessageSchema(
        subject=subject,
        recipients=to_addr,
        body=body,
        subtype=MessageType.html,
    )

    await mail.send_message(message)


def render_email_from_jinja2(tempalte_path: str, context: Any):
    template = jinja_env.get_template(tempalte_path)
    rendered_string = template.render(**context)
    return rendered_string