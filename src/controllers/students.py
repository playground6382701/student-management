from uuid import uuid1
from sqlalchemy import or_

from src.connections import postgresql
from src.schemas import Student, User, Department
from src.payloads.request import CreateStudentPayload
from src.utils.security import get_password_hash, create_activation_code
from src.payloads import ListStudentPayload, UpdateStudentPayload
from .email import send_email, render_email_from_jinja2
from .cloudinary import upload_file
from settings import settings


def get_student(payload: dict = {}):
    session = postgresql.get_session()
    # search = payload.get("search")
    # if search:
    #     del payload["search"]
    query = [getattr(User, key)==value for key, value in payload.items() if hasattr(User)]
    
    # if search:
    #     query.append(
    #         or_(
    #             User.first_name.ilike(search),
    #             User.last_name.ilike(search),
    #         )
    #     )
    results = session.query(Student, User, Department).join(User).join(Department).where(
        *query
    ).all()
    results = [{"student": student, "user": user, "department": department} for student, user, department in results]
    return results


async def create_student(payload: CreateStudentPayload):
    session = postgresql.get_session()
    user = session.query(User).where(User.username==payload.username).first()
    if user:
        raise Exception("Username đã tồn tại")
    try:
        raw_password = uuid1().hex
        hashed_password = get_password_hash(raw_password)
        user = User(
            username=payload.username,
            password=hashed_password,
            code=str(uuid1()),
            email=payload.email,
            first_name=payload.first_name,
            last_name=payload.last_name,
        )
        session.add(user)
        session.flush()

        student = Student(
            user_id=user.id,
            department_id=payload.department,
        )
        session.add(student)


        activation_code = create_activation_code({
                "username": user.username, 
                "password": raw_password,
            })
        email_body = render_email_from_jinja2(
            "email/account_activation.html",
            {
                "username": user.username,
                "activation_link": f"//{settings.SITE_DNS}/activate_user/{activation_code}",
            }
        )
        await send_email(payload.email, "Account Created", email_body)
    except Exception as e:
        print(e)
    else:
        session.commit()


def update_student(payload: UpdateStudentPayload):
    session = postgresql.get_session()
    user = session.query(User).where(User.id==payload.id).first()
    student = session.query(Student).where(Student.id==user.id).first()
    try:

        user.first_name = payload.first_name
        user.last_name = payload.last_name
        student.department_id = payload.department

        if payload.file:
            response = upload_file(payload.file)
            print(response)
            student.profile_image_url = response.get("secure_url")

    except Exception as e:
        print(e)
    else:
        session.commit()


def deactivate_student(id):
    session = postgresql.get_session()
    user = session.query(User).where(User.id==id).first()
    user.is_active = False
    
    session.commit()