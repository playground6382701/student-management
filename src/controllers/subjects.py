from uuid import uuid1
from sqlalchemy import select
from src.connections import postgresql
from src.schemas import Subject, Department, StudentSubject, Student
from src.payloads import CreateSubjectPayload, UpdateSubjectPayload


def create_subject(payload: CreateSubjectPayload):
    session = postgresql.get_session()
    department = session.query(Department).where(Department.id==payload.department_id).first()
    if payload.credit > department.required_credit:
        raise Exception("Số tín chỉ của môn không được lớn hơn số tín chỉ cần để tốt nghiệp")
    try:
        subject = Subject(
            name=payload.name,
            codename=payload.codename,
            department_id=payload.department_id,
            credit=payload.credit,
        )
        session.add(subject)
    except Exception as e:
        print(e)
    else:
        session.commit()
        return subject
    


def get_subject(payload: dict = {}):
    session = postgresql.get_session()
    query = [getattr(Subject, key)==value for key, value in payload.items()]
    results = session.query(Subject, Department).join(Department).where(*query).all()
    results = [{"subject": subject, "department": department} for subject, department in results]
    return results

def get_subject_by_department_id(departmentID: int):
    session = postgresql.get_session()
    subject = session.query(Subject).where(Subject.department_id==departmentID).all()
    return subject


def get_student_subject(student_id: int):
    session = postgresql.get_session()

    results = session.query(Subject, StudentSubject, Student, Department) \
    .join(StudentSubject, StudentSubject.student_id == Student.id) \
    .join(Subject, Subject.id == StudentSubject.subject_id) \
    .join(Department, Department.id == Student.department_id) \
    .where(Student.id == student_id) \
    .all()
    

    # results = [{"subject": subject, "student_subject": student_subject, "student": student} for subject, student_subject, student in results]
    # print(results)
    return results


def update_subject(payload: UpdateSubjectPayload):
    session = postgresql.get_session()

    subject = session.query(Subject).where(Subject.codename==payload.codename).first()
    if subject and subject.id != payload.id:
        raise Exception("exsted subject with same codename")
    try:
        subject = session.query(Subject).where(Subject.id==payload.id).first()
        subject.name = payload.name
        if subject.codename != payload.codename:
            subject.codename = payload.codename

        if subject.department_id != payload.department_id:
            subject.department_id = payload.department_id
        
        if subject.credit != payload.credit:
            subject.credit = payload.credit
    except subject as e:
        print(e)
    else:
        session.commit()
        return subject
    

def get_subject_detail(id: int):
    session = postgresql.get_session()

    results = session.query(Student, StudentSubject, Subject) \
    .join(StudentSubject, StudentSubject.student_id == Student.id) \
    .join(Subject, Subject.id == StudentSubject.subject_id) \
    .where(Subject.id == id) \
    .all()
    

    # results = [{"subject": subject, "student_subject": student_subject, "student": student} for subject, student_subject, student in results]
    # print(results)
    return results


def delete_subject(id: str):
    session = postgresql.get_session()
    try:
        department = session.scalar(select(Subject).where(Subject.id==id))
        print(department)
        if department:
            session.delete(department)
    except Exception as e:
        print(e)
    else:
        session.commit()


def update_student_grade(
    student_id: int,
    subject_id: int,
    grade: float,
    note: str,
):
    session = postgresql.get_session()
    student_subject = session.query(StudentSubject).where(
        StudentSubject.subject_id==subject_id, 
        StudentSubject.student_id==student_id
    ).first()
    student_subject.grade = grade
    student_subject.note = note
    session.commit()


def get_grade(payload: dict= {}):
    session = postgresql.get_session()
    subject_id = payload.get("subject_id")
    student_id = payload.get("student_id")
    student_subject = session.query(StudentSubject).where(
        StudentSubject.subject_id==subject_id, 
        StudentSubject.student_id==student_id
    ).first()
    return student_subject