from src.connections import postgresql
from src.schemas import User
from src.utils import security


def create_new_user(username: str, password: str, info: dict):
    hashed_password = security.get_password_hash(password)
    new_user = User(
        username=username,
        password=hashed_password,
        **info
    )

    session = postgresql.get_session()
    session.add(new_user)
    session.commit()


def query_user(username: str):
    session = postgresql.get_session()
    user = session.query(User).filter_by(username=username).first()
    return user