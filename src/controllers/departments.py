from uuid import uuid1
from sqlalchemy import select, delete

from src.payloads import CreateDepartmentPayload, UpdateDepartmentPayload
from src.schemas import Department, Student

from src.connections import postgresql


def is_department_existed(codename: str):
    session = postgresql.get_session()
    depertment = session.scalar(select(Department).where(Department.codename==codename))
    if depertment:
        return True
    return False

def create_department(payload: CreateDepartmentPayload):
    session = postgresql.get_session()
    try:
        code = f"{payload.codename}{uuid1().hex}"[:32]
        department = Department(
            name=payload.name,
            codename=payload.codename,
            code=code,
            required_credit=payload.required_credit,
        )
        session.add(department)
    except Exception as e:
        print(e)
    else:
        session.commit()
        return department
    

def get_department(payload: dict = {}):
    session = postgresql.get_session()
    query = [getattr(Department, key)==value for key, value in payload.items()]
    departments = session.query(Department).where(*query).all()
    return departments
    
def get_department_by_student_id(studentID: int):
    session = postgresql.get_session()
    department = session.query(Department).join(Student).where(Student.id==studentID).first()
    return department
def update_department(payload: UpdateDepartmentPayload):
    session = postgresql.get_session()
    try:
        department: Department = session.query(Department).where(Department.id==payload.id).first()
        department.name = payload.name
        if department.codename != payload.codename:
            department.codename = payload.codename
            code = f"{payload.codename}{uuid1().hex}"[:32]
            department.code = code
        department.required_credit = payload.required_credit
    except Exception as e:
        print(e)
    else:
        session.commit()
        return department
    

def delete_department(id: int):
    session = postgresql.get_session()
    try:
        department = session.scalar(select(Department).where(Department.id==id))
        print(department)
        if department:
            session.delete(department)
    except Exception as e:
        print(e)
    else:
        session.commit()
    