from src.payloads import ActivateUserPayload
from src.schemas import User
from src.connections import postgresql
from src.utils.security import verify_password, get_password_hash, decrypt_activation_code


def activate_user(payload: ActivateUserPayload):
    session = postgresql.get_session()
    data = decrypt_activation_code(payload.activation_code)
    user = session.query(User).where(User.username==data.get("username")).first()
    is_password_matched = verify_password(data.get("password"), user.password)
    if is_password_matched:
       hashed_password = get_password_hash(payload.password)
       user.password = hashed_password
       user.is_active = True
       session.commit()
