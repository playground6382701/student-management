from cloudinary.uploader import upload


def upload_file(file):
    try:
        # Upload file to Cloudinary
        result = upload(file)
    except Exception as e:
        print(e)
        raise Exception("không thể upload file")
    return result
