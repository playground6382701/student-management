from fastapi_mail import ConnectionConfig, FastMail

from settings import settings


conf = ConnectionConfig(
    MAIL_SERVER=settings.MAIL_HOST,
    MAIL_PORT=settings.MAIL_PORT,
    MAIL_USERNAME=settings.MAIL_USER,
    MAIL_PASSWORD=settings.MAIL_PSWD,
    MAIL_FROM=settings.MAIL_USER,
    MAIL_STARTTLS=False,
    MAIL_SSL_TLS=True,
    USE_CREDENTIALS=True,
    VALIDATE_CERTS=True
)

mail = FastMail(conf)
