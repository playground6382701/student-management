from typing import Dict
from sqlalchemy.orm import Session
from sqlalchemy.engine import create_engine, Engine, Connection
from sqlalchemy.exc import OperationalError
from settings import settings


def get_engine(
) -> Engine:
    db_uri = f"postgresql+psycopg2://{settings.DB_USER}:{settings.DB_PSWD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
    engine = create_engine(db_uri)
    return engine


def get_session() -> Session:
    engine = get_engine()
    session = Session(engine)
    return session


def get_connection() -> Connection:
    engine = get_engine()
    connection = engine.connect()
    return connection