FROM python3.11-bullseye-slim

WORKDIR /usr/bin/app

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT [ "sh/start.sh" ]