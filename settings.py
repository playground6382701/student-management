from pydantic_settings import BaseSettings
from pydantic_settings.sources import PydanticBaseSettingsSource
import cloudinary


class Settings(BaseSettings, extra="allow"):
    DB_HOST: str
    DB_PORT: int
    DB_USER: str
    DB_PSWD: str
    DB_NAME: str

    MAIL_HOST: str
    MAIL_PORT: str
    MAIL_USER: str
    MAIL_PSWD: str

    SECRET_KEY: str
    SITE_DNS: str

    CLOUDINARY_CLOUD_NAME: str
    CLOUDINARY_API_KEY: str
    CLOUDINARY_API_SECRET: str

    class Config:
        env_file = ".env"

    @classmethod
    def settings_customise_sources(
        cls, 
        settings_cls: type[BaseSettings], 
        init_settings: PydanticBaseSettingsSource, 
        env_settings: PydanticBaseSettingsSource, 
        dotenv_settings: PydanticBaseSettingsSource, 
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return dotenv_settings, init_settings, env_settings, file_secret_settings

settings = Settings()

cloudinary.config(
    cloud_name=settings.CLOUDINARY_CLOUD_NAME,
    api_key=settings.CLOUDINARY_API_KEY,
    api_secret=settings.CLOUDINARY_API_SECRET,
)