from fastapi import FastAPI
from fastapi.templating import Jinja2Templates
from fastapi.requests import Request

from src.routers import ROUTERS
from src.events import setup_startup

app = FastAPI()


templates = Jinja2Templates(directory='templates')


for router in ROUTERS:
    app.include_router(router)


@app.get("/")
async def root(request: Request):
    context = {
        "request": request
    }
    return templates.TemplateResponse("home.html", context)


@app.on_event("startup")
async def startup():
    setup_startup()